<?php

/**
 * @file
 * Install, update and uninstall functions for the openculturas_map module.
 */

declare(strict_types=1);

use Drupal\Core\Field\FieldConfigInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\user\RoleInterface;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;

/**
 * Implements hook_install().
 */
function openculturas_map_install(bool $is_syncing): void {
  if ($is_syncing) {
    return;
  }

  /** @var \Drupal\Core\Field\FieldConfigInterface|null $field */
  $field = FieldConfig::loadByName('paragraph', 'block', 'field_block_ref');
  if ($field instanceof FieldConfigInterface) {
    $selection_settings = $field->getSetting('selection_settings');
    if (is_array($selection_settings) && $field->getSetting('selection') === 'categories') {
      $selection_settings['categories']['OpenCulturas - Map'] = 'OpenCulturas - Map';
    }
    elseif (is_array($selection_settings) && $field->getSetting('selection') === 'blocks') {
      $selection_settings['plugin_ids']['openculturas_map_block'] = 'openculturas_map_block';
    }

    $field->setSetting('selection_settings', $selection_settings);
    $field->save();
  }

  /** @var \Drupal\user\RoleStorageInterface $roleStorage */
  $roleStorage = \Drupal::entityTypeManager()->getStorage('user_role');
  /** @var \Drupal\user\RoleInterface|null $role */
  $role = $roleStorage->load('oc_admin');
  if ($role instanceof RoleInterface) {
    $role->grantPermission('administer openculturas_map configuration');
    $role->save();
  }

}

/**
 * Implements hook_uninstall().
 */
function openculturas_map_uninstall(bool $is_syncing): void {
  if ($is_syncing) {
    return;
  }

  /** @var \Drupal\Core\Field\FieldConfigInterface|null $field */
  $field = FieldConfig::loadByName('paragraph', 'block', 'field_block_ref');
  if ($field instanceof FieldConfigInterface) {
    $selection_settings = $field->getSetting('selection_settings');
    if (is_array($selection_settings) && $field->getSetting('selection') === 'categories') {
      unset($selection_settings['categories']['OpenCulturas - Map']);
    }
    elseif (is_array($selection_settings) && $field->getSetting('selection') === 'blocks') {
      unset($selection_settings['plugin_ids']['openculturas_map_block']);
    }

    $field->setSetting('selection_settings', $selection_settings);
    $field->save();
  }
}

/**
 * Implements hook_update_last_removed().
 */
function openculturas_map_update_last_removed(): int {
  return 10002;
}

/**
 * Set pager type of the rest export display to mini with 60 items.
 */
function openculturas_map_update_10003(): void {
  $set_pager = static function (ViewExecutable $view): void {
    $display = $view->getDisplay();
    $pager_plugin_manager = Views::pluginManager('pager');
    if ($pager_plugin_manager->hasDefinition('mini')) {
      /** @var \Drupal\views\Plugin\views\pager\Mini $new_pager */
      $new_pager = $pager_plugin_manager->createInstance('mini');
      $new_pager->init($view, $display);
      $new_pager->setItemsPerPage(60);
      $display->setOption('pager', [
        'type' => $new_pager->getPluginId(),
        'options' => $new_pager->options,
      ]);
      $view->save();
    }

  };
  $view = Views::getView('oc_map_locations');
  if ($view && $view->setDisplay('rest_export')) {
    $set_pager($view);
  }

  $view = Views::getView('oc_map_dates');
  if ($view && $view->setDisplay('rest_export_1')) {
    $set_pager($view);
  }
}
